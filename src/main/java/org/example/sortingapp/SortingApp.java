package org.example.sortingapp;

import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SortingApp {
    private static final Logger logger = LogManager.getLogger(SortingApp.class);
    public static int[] numbers;

    public static void main(String[] args) {
        numbers = parseArguments(args);

        logger.info("Input numbers: {}", Arrays.toString(numbers));

        Arrays.sort(numbers);

        logger.info("Sorted numbers: {}", Arrays.toString(numbers));

        printNumbers(numbers);
    }

    private static int[] parseArguments(String[] args) {
        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            numbers[i] = Integer.parseInt(args[i]);
        }
        return numbers;
    }

    private static void printNumbers(int[] numbers) {
        logger.info("Sorted numbers printed:");

        for (int number : numbers) {
            System.out.println(number);
        }
    }
}
