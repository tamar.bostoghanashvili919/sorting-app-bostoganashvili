package org.example.sortingapp.org.example;

import org.example.sortingapp.SortingApp;
import org.junit.Test;
import org.junit.BeforeClass;
import org.testng.annotations.AfterClass;

import static org.junit.Assert.assertArrayEquals;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class SortingAppTest {
    private static ByteArrayOutputStream consoleOutput;
    private static PrintStream originalOutput;
    private static int[] numbers;

    @BeforeClass
    public static void setup() {
        consoleOutput = new ByteArrayOutputStream();
        originalOutput = System.out;
        System.setOut(new PrintStream(consoleOutput));
    }

    @Test
    public void testSortingAppWithZeroArguments() {
        String[] args = {};
        int[] expected = {};
        SortingApp.main(args);
        assertArrayEquals(expected, SortingApp.numbers);
        String console = consoleOutput.toString().trim();
        assert console.contains("Input numbers: []");
        assert console.contains("Sorted numbers: []");
    }

    @Test
    public void testSortingAppWithOneArgument() {
        String[] args = { "5" };
        int[] expected = { 5 };
        SortingApp.main(args);
        assertArrayEquals(expected, SortingApp.numbers);
        String console = consoleOutput.toString().trim();
        assert console.contains("Input numbers: [5]");
        assert console.contains("Sorted numbers: [5]");
    }

    @Test
    public void testSortingAppWithTenArguments() {
        String[] args = { "5", "3", "8", "1", "9", "2", "7", "6", "4", "0" };
        int[] expected = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        SortingApp.main(args);
        assertArrayEquals(expected, SortingApp.numbers);
        String console = consoleOutput.toString().trim();
        assert console.contains("Input numbers: [5, 3, 8, 1, 9, 2, 7, 6, 4, 0]");
        assert console.contains("Sorted numbers: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]");
    }

    @Test
    public void testSortingAppWithMoreThanTenArguments() {
        String[] args = { "5", "3", "8", "1", "9", "2", "7", "6", "4", "0", "10", "11" };
        int[] expected = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };


        consoleOutput.reset();

        SortingApp.main(args);
        assertArrayEquals(expected, SortingApp.numbers);
        String console = consoleOutput.toString().trim();
        assert console.contains("Input numbers: [5, 3, 8, 1, 9, 2, 7, 6, 4, 0, 10, 11]");
        assert console.contains("Sorted numbers: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]");
    }

    private static void main(String[] args) {
    }


    @AfterClass
    public static void cleanup() {
        System.setOut(originalOutput);
    }
}
